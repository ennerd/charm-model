<?php
namespace Charm;

use Charm\Recordset\RecordsetInterface;
use Charm\Event\StaticEventEmitterTrait;

abstract class Model {
    use StaticEventEmitterTrait;

    abstract protected static function getRows(): RecordsetInterface;

    public static function getPrimaryKeyName(): string {
        return 'id';
    }

    /**
     * Returns a map of foreign key name to model class name. This is required
     * for performing joins.
     *
     * @return array<string, Model>
     */
    public static function getForeignKeys(): array {
        return [];
    }

    public final static function all(): RecordsetInterface {
        return static::getRows();
    }

    public final function load($id): static {
        return current($this->all->eq(static::getPrimaryKeyName(), $id)->page(0, 1));
    }

    public final function save(): bool {
        throw new \Exception("Model::save() is not implemented yet.");
    }

    public final function delete(): bool {
        throw new \Exception("Model::delete() is not implemented yet.");
    }
}
